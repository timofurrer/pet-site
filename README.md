# pet-site Demo

This project demonstrates the use of a Terraform module deployed to the GitLab Infrastructure Registry.

The pipeline of this project deploys the *pet-site* into three tiers: `production`, `staging` and `testing`.
Each tier is deployed using its own child-pipeline which is associated with a GitLab Environment, Deployments
and GitLab-managed Terraform state.